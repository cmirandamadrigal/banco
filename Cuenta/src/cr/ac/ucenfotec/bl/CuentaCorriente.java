package cr.ac.ucenfotec.bl;

public class CuentaCorriente extends Cuenta{

    private double cuotaPorOperacion;

    public CuentaCorriente() {
        super();
    }

    public CuentaCorriente(double depositoInicial) {
        super(depositoInicial);
        this.cuotaPorOperacion = 0.10;
    }

    public double getCuotaPorOperacion() {
        return cuotaPorOperacion;
    }

    public void setCuotaPorOperacion(double cuotaPorOperacion) {
        this.cuotaPorOperacion = cuotaPorOperacion;
    }

    @Override
    public void depositarSaldo(double deposito){
        this.saldo += deposito;
    }

    @Override
    public void retirarSaldo(double retiro){
        this.saldo -= retiro;
        cobrarComisiones(retiro);
    }

    @Override
    public void cobrarComisiones(double monto) {
        this.saldo -= (monto * cuotaPorOperacion);
    }

    @Override
    public String toString() {
        return "CuentaCorriente{" +
                "numero=" + numero +
                ", saldo=" + saldo +
                ", fechaDeCreacion=" + fechaDeCreacion +
                fechaDeCreacion.getDayOfMonth() + "/" +
                fechaDeCreacion.getMonthValue() + "/" +
                fechaDeCreacion.getYear() +
                '}';
    }
}
