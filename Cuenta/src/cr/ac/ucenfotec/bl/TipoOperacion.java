package cr.ac.ucenfotec.bl;

public enum TipoOperacion {

    RETIRO("R"), DEPOSITO("D");

    private TipoOperacion(String nombre) {

        this.nombre = nombre;
        this.abreviatura = nombre.charAt(0);
    }


    public char getAbreviatura() {
        return abreviatura;
    }
    public String getNombre() {
        return nombre;
    }

    private char abreviatura;
    private String nombre;
}
