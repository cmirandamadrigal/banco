package cr.ac.ucenfotec.bl;

import java.util.ArrayList;

public class Cliente {

    private String nombre, cedula, direccion;
    private ArrayList<Cuenta> cuentas;

    public Cliente() {
    }

    public Cliente(String nombre, String cedula, String direccion, ArrayList<Cuenta> cuentas) {
        this.nombre = nombre;
        this.cedula = cedula;
        this.direccion = direccion;
        this.cuentas = cuentas;
    }

    public Cliente(String nombre, String cedula, String direccion) {
        this.nombre = nombre;
        this.cedula = cedula;
        this.direccion = direccion;
        this.cuentas = new ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public ArrayList<Cuenta> getCuentas() {
        return cuentas;
    }

    public void setCuentas(ArrayList<Cuenta> cuentas) {
        this.cuentas = cuentas;
    }

    public void agregarCuenta(Cuenta cuenta){
        this.cuentas.add(cuenta);
    }

    @Override
    public String toString() {
        String cuentas = "No hay cuentas";

        if(this.cuentas.size() > 0){
            for(Cuenta tmp : this.cuentas) {
                cuentas += tmp.toString();
            }
        }

        return "Cliente{" + "nombre='" + nombre + ", cedula='" + cedula + ", direccion='" + direccion +
                ", cuentas=" + cuentas + '}';
    }
}
