package cr.ac.ucenfotec.bl;

import java.time.LocalDate;

public class CuentaAhorro extends Cuenta{

    private double cuotaMantenimiento;

    public CuentaAhorro() {
        super();
    }

    public CuentaAhorro(double depositoInicial) {
        super(depositoInicial);
        this.cuotaMantenimiento = 0.15;
    }

    @Override
    public void depositarSaldo(double deposito) {
        this.saldo += deposito;
    }

    @Override
    public void retirarSaldo(double retiro) {
        this.saldo -= retiro;
        cobrarComisiones(retiro);
    }

    @Override
    public void cobrarComisiones(double monto) {

        LocalDate ya = LocalDate.now();

        int mesesDiferenciaCreacion = ya.getMonthValue() - fechaDeCreacion.getMonthValue();

        for (int i = 0; i < mesesDiferenciaCreacion; i++){
            this.saldo -= (saldo/mesesDiferenciaCreacion) * cuotaMantenimiento;
        }
    }


    @Override
    public String toString() {
        return "CuentaAhorro{" +
                "cuotaMantenimiento=" + cuotaMantenimiento +
                ", numero=" + numero +
                ", saldo=" + saldo +
                ", fechaDeCreacion=" +
                fechaDeCreacion.getDayOfMonth() + "/" +
                fechaDeCreacion.getMonthValue() + "/" +
                fechaDeCreacion.getYear() +
                '}';
    }
}
