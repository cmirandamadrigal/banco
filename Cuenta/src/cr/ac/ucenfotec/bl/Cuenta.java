package cr.ac.ucenfotec.bl;

//import java.time.LocalDateTime;

import java.time.LocalDate;
import java.util.ArrayList;

public abstract class Cuenta{

    protected int numero = 0;
    protected double saldo;
    protected LocalDate fechaDeCreacion;
    protected ArrayList<Operacion> operaciones;

    //Mensajes que se muestran por cada retiro

    public Cuenta(){
        operaciones = new ArrayList<>();
    }

    public Cuenta(double depositoInicial){
//        this.numero = (int)(Math.random()*(999999-000000+1)+000000);
        this.numero++;
        this.saldo = depositoInicial;
        this.fechaDeCreacion = LocalDate.now();
        this.operaciones = new ArrayList<>();
    }

    public Cuenta(double depositoInicial,ArrayList<Operacion> listaOperaciones){
//        this.numero = (int)(Math.random()*(999999-000000+1)+000000);
        this.numero++;
        this.saldo = depositoInicial;
        this.fechaDeCreacion = LocalDate.now();
        this.operaciones = listaOperaciones;
    }

    public int getNumero(){
        return numero;
    }

    public double getSaldo(){
        return saldo;
    }

    public LocalDate getFechaDeCreacion(){
        return fechaDeCreacion;
    }

    public abstract void depositarSaldo(double deposito);

    public abstract void retirarSaldo(double retiro);

    public abstract void cobrarComisiones(double monto);

    public void registrarOperacion(Operacion operacion){
        this.operaciones.add(operacion);
    }
}
