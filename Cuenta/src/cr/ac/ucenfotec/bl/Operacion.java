package cr.ac.ucenfotec.bl;

import java.time.LocalDate;

public class Operacion {

    private int numero = 0;
    private double monto;
    private LocalDate fecha;
    private TipoOperacion tipo;

    public Operacion(double monto, String tipo) {
        numero++;
        this.monto = monto;
        this.tipo = TipoOperacion.valueOf(tipo);
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public TipoOperacion getTipo() {
        return tipo;
    }

    public void setTipo(TipoOperacion tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Operacion{" +
                "numero=" + numero +
                ", monto=" + monto +
                ", fecha=" + fecha.getDayOfMonth() +
                "/" + fecha.getMonthValue() +
                "/" + fecha.getYear() +
                ", tipo=" + tipo.name() +
                '}';
    }
}
