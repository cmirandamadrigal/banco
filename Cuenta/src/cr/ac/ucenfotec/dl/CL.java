package cr.ac.ucenfotec.dl;

import cr.ac.ucenfotec.bl.Cliente;
import cr.ac.ucenfotec.bl.Cuenta;
import java.util.ArrayList;

public class CL {

    private ArrayList<Cliente> clientes;

//    private ArrayList<String> ids_transferencias = new ArrayList<>(cuentas.size());

    public CL(){
        clientes = new ArrayList<>();
    }

    public ArrayList<Cliente> getClientes(){
        return clientes;
    }

    public void agregarCliente(Cliente nuevoCliente){
        clientes.add(nuevoCliente);
    }

    public void agregarCuenta(int indice, Cuenta nuevaCuenta){
        clientes.get(indice).agregarCuenta(nuevaCuenta);
    }

    public int validarClientePorCedula(String cedula){
        byte existe = -1;

        byte i = 0;

        for(Cliente tmpCliente : clientes){

            String tmpCedula = tmpCliente.getCedula();

            if(tmpCedula.equals(cedula)){
                existe = i;
            }
            i++;
        }

        return existe;
    }

    public void depositarSaldo(int indiceCliente, int indiceCuenta, double monto){
        clientes.get(indiceCliente).getCuentas().get(indiceCuenta).depositarSaldo(monto);
    }

    public int validarCuentaPorNumero(String cedula, int numeroCuenta){
        byte existe = -1;

        byte i = 0;

        for(Cliente tmpCliente : clientes){

            String tmpCedula = tmpCliente.getCedula();

            if(tmpCedula.equals(cedula)){
                for(Cuenta tmpCuenta : tmpCliente.getCuentas()){
                    if(tmpCuenta.getNumero() == numeroCuenta){
                        existe = i;
                    }
                }
            }
            i++;
        }

        return existe;
    }
}
