package cr.ac.ucenfotec.ui;

//import cr.ac.ucenfotec.dl.CL;
import cr.ac.ucenfotec.tl.Controller;

        import java.io.*;

public class UI {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    static PrintStream out = System.out;

    static Controller controller = new Controller();

//    static CL logica = new CL();

    public static void main(String[] args) throws IOException{
        mostrarMenu();
    }

    public static void mostrarMenu() throws IOException {

        int opcion = -1;

        do {
            out.println("\n1. Registrar cuenta");
            out.println("2. Listar cuentas");
            out.println("3. Hacer un depósito");
            out.println("4. Hacer un retiro");
            out.println("0. Salir\n");

            out.println("Digite una opción\n");

            opcion = Integer.parseInt(in.readLine());

            procesarOpcion(opcion);

        }while (opcion != 0);

    }

    public static void procesarOpcion(int popcion) throws IOException {

        switch (popcion){
            case 1:
                registrarCuenta();
                break;
            case 2:
                listarCuentas();
                break;
            case 3:
                depositarSaldo();
                break;
            case 4:
                retirarSaldo();
                break;
            case 0:
                out.println("\nGracias por usar nuestro sistema bancario.\n");
                break;
            default:
                out.println("\nOpción inválida.\n");

        }
    }

    private static void registrarCuenta() throws IOException{
        int numero;
        double saldo;
        int dia, mes, anio;
        String duenio;

        out.println("Escriba el número de la cuenta");
        numero = Integer.parseInt(in.readLine());

        out.println("Escriba el saldo de la cuenta");
        saldo = Double.parseDouble(in.readLine());

        out.println("Escriba el día de creación de la cuenta");
        dia = Integer.parseInt(in.readLine());

        out.println("Escriba el mes de creación de la cuenta");
        mes = Integer.parseInt(in.readLine());

        out.println("Escriba el año de creación de la cuenta");
        anio = Integer.parseInt(in.readLine());

        out.println("Escriba el dueño de la cuenta");
        duenio = in.readLine();

        out.println(controller.registrarCuenta(numero, saldo, anio, mes, dia
                , duenio));
    }

    private static void listarCuentas() throws IOException{
        String[] listCuentas = controller.listarCuentas();

        byte i = 0;
        for(String dataCuenta : listCuentas){
            out.println("\n" + dataCuenta + "\n");
            i++;
        }

    }

//    private static void listarCuentas() throws IOException{
//        ArrayList<Cuenta> listCuentas = controller.listarCuentas();
//
//        for(Cuenta dataCuenta : listCuentas){
//            out.println("\n" + dataCuenta.formateado(false) + "\n");
//        }
//
//        listCuentas.add(new Cuenta(20000.0, 2000, 12, 12, "Roberta"));
//
//        out.println("Reimpresión con la cuenta agregada");
//
//        for (Cuenta tmpCuenta:
//             listCuentas) {
//            out.println(("\n" + tmpCuenta.formateado(false) + "\n"));
//        }
//
//        controller.imprimirLista(listCuentas, out);
//    }


    private static void depositarSaldo() throws IOException{
        int numero;
        double deposito;
        boolean existe = true;

        out.println("Escriba el número de la cuenta a la que desea depositar");
        numero = Integer.parseInt(in.readLine());

        out.println(controller.getSaldoPorNumeroDeCuenta(numero));

        if(controller.getSaldoPorNumeroDeCuenta(numero).equals("\nLa persona no existe.\n")) existe = false;

        if(existe){
            out.println("Escriba el monto a depositar a la cuenta con el número " + numero);
            deposito = Double.parseDouble(in.readLine());

            out.println(controller.depositarSaldo(numero, deposito));
        }

    }

    private static void retirarSaldo() throws IOException{
        int numero;
        double retiro;

        boolean existe = true;
        out.println("Escriba el número de la cuenta de la que desea retirar");
        numero = Integer.parseInt(in.readLine());

        String saldoCuenta = controller.getSaldoPorNumeroDeCuenta(numero);

        out.println(saldoCuenta);

        if(saldoCuenta.equals("\nLa persona no existe.\n")) existe = false;

        if(existe && !saldoCuenta.equals("\nEl saldo actual de la persona es 0.0\n")) {
            out.println("Escriba el monto a retirar de la cuenta con el núnero " + numero);

            retiro = Double.parseDouble(in.readLine());

            out.println(controller.retirarSaldo(numero, retiro));
        }
        if(saldoCuenta.equals("\nEl saldo actual de la persona es 0.0\n")){
            out.println("\nNo puede hacer retiros de esa cuenta.");
        }
    }

//    private static void transferirSaldo() throws IOException{
//        int numero
//    }


}
