package cr.ac.ucenfotec.tl;

import cr.ac.ucenfotec.bl.Cuenta;
import cr.ac.ucenfotec.bl.CuentaCorriente;
import cr.ac.ucenfotec.dl.CL;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;

public class Controller {


    CL logica = new CL();

    public String registrarCuentaCorriente(double saldo, String cedula){

        Cuenta tmpCuenta = new CuentaCorriente(saldo);//constructor con número de cuenta ingresado por el usuario

        int existe = logica.validarClientePorCedula(cedula);

        String mensaje = mensajeRegistro(existe, saldo);

        if(existe == -1 && saldo >= 0){
            logica.agregarCuenta(existe, tmpCuenta);
        }
        return mensaje;
    }

    public String[] listarCuentas(String cedula){

        if(logica.validarClientePorCedula(cedula) != -1){
            ArrayList<Cuenta> tmpCuentasAL = logica.getClientes().get(logica.validarClientePorCedula(cedula)).getCuentas();

            String[] tmpCuentasA = new String[tmpCuentasAL.size()];

            if(tmpCuentasAL.size() > 0){
                byte i = 0;
                for (Cuenta tmpCuenta : tmpCuentasAL){
                    tmpCuentasA[i] = tmpCuenta.toString();
                    i++;
                }
            } else {
                tmpCuentasA = new String[1];
                tmpCuentasA[0] = "No hay cuentas registradas.";
            }
            return tmpCuentasA;
        } else {
            String[] error = {"No se encontro el cliente."};
            return error;
        }
    }

    public String depositarSaldo(int numero, double deposito){

        byte existe = logica.validarCuentaPorNumero(numero);

        String mensaje = mensajeDeposito(existe, deposito);

        if(existe != -1 && deposito >0){
            mensaje += logica.depositarSaldo(numero, deposito);
        }

        return mensaje;
    }

    public String retirarSaldo(int numero, double retiro){

        byte existe = logica.validarCuentaPorNumero(numero);

        ArrayList<Cuenta> tmpCuentas = logica.getCuentas();

        boolean saldoSuficiente = true;

        Cuenta tmpCuenta = tmpCuentas.get(existe);

        if(retiro > tmpCuenta.getSaldo()){
            saldoSuficiente = false;
        }

        String mensaje = mensajeRetiro(existe, retiro, saldoSuficiente);

        if(existe != -1 && retiro >0 && saldoSuficiente){
            mensaje += logica.retirarSaldo(numero, retiro);
        }

        return mensaje;
    }

    public String getSaldoPorNumeroDeCuenta(int numero){

        double saldo = logica.getSaldoPorNumeroDeCuenta(numero);

        String mensaje;

        if(saldo != -1){
            mensaje = "\nEl saldo actual de la persona es " + saldo + "\n";
        } else {
            mensaje = "\nLa persona no existe.\n";
        }

        return mensaje;
    }

    public String mensajeRegistro(int existe, double saldo){
        String mensaje, mensaje1 = "Registro exitoso de la cuenta.", mensaje2 = "Registro fallido de la cuenta. ";

        if(existe == -1 && saldo >=0){
            mensaje = mensaje1;
        } else {
            mensaje = mensaje2;
        }
        if(existe != -1){
            mensaje+= "El registro ya existe.";
        }
        if(saldo<0){
            mensaje += "El saldo no puede ser negativo.";
        }

        return "\n" + mensaje + "\n";
    }

    public String mensajeDeposito(int existe, double deposito){
        String mensaje, mensaje1 = "Depósito exitoso a la cuenta.", mensaje2 = "Depósito fallido.";

        if(existe != -1 && deposito >0){
            mensaje = mensaje1;
        }  else {
            mensaje = mensaje2;
        }

        if (existe == -1){
        mensaje += " La cuenta con ese número ya existe.";
        }

        if(deposito <= 0){
        mensaje += " El depósito no puede ser 0 o negativo.";
        }

        return "\n" + mensaje + "\n";
    }

    public String mensajeRetiro(int existe, double retiro, boolean saldoSuficiente){
        String mensaje, mensaje1 = "Retiro exitoso de la cuenta.", mensaje2 = "Retiro fallido. ";

        if(existe != -1 && retiro >0 && saldoSuficiente){
            mensaje = mensaje1;
        } else {
            mensaje = mensaje2;
        }

        if(existe == -1){
            mensaje += "No existe registro de la cuenta destinataria del retiro.";
        }
        if(retiro <= 0){
            mensaje += "El monto a retirar no puede ser 0 o negativo.";
        }
        if(!saldoSuficiente){
            mensaje += "El saldo de la cuenta de la cual retirar " + retiro + " es insuficiente.";
        }
        return "\n" + mensaje + "\n";
    }
}
